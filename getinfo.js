(async function (sessionInfo) {
	let apiBase = sessionInfo.ApiGatewayUri;
	let apiVersion = sessionInfo.ApiGatewayVersion;
	let apiAccessToken = sessionInfo.AccessToken;

	async function api(path, query = {}) {
		let url = new URL(path, apiBase);
		url.search = `api-version=${apiVersion}`;
		for (let k of Object.keys(query)) {
			let v = query[k];
			url.search += `&${encodeURIComponent(k)}=${encodeURIComponent(v)}`;
		}
		let headers = new Headers();
		headers.set("Authorization", `Bearer ${apiAccessToken}`);
		headers.set("Accept", "application/json");
		let res = await fetch(url.toString(), {
			method: "GET",
			headers
		});
		if (res.status !== 200) {
			throw new Error(`Server responded with ${res.status}.`);
		}
		return await res.json();
	}

	async function getText(path) {
		let res = await fetch(path, {method: "GET"});
		if (res.status !== 200) {
			throw new Error(`Server responded with ${res.status}`);
		}
		return await res.text();
	}

	let videoId = window.location.toString().match(/\/video\/(.+)$/)[1];
	let videoInfo = await api(`videos/${encodeURIComponent(videoId)}`);
	let manifestUrl = videoInfo.playbackUrl + "(format=mpd-time-csf)";
	console.log(`Downloading manifest from ${manifestUrl}`);
	let manifestText = await getText(manifestUrl);
	let manifestDoc = new DOMParser().parseFromString(manifestText, "application/xml");

	let mediaSummary = {
		streams: [],
		mainfest: manifestUrl,
		name: videoInfo.name,
		description: videoInfo.description
	};
	if (manifestDoc.documentElement.tagName !== "MPD") {
		throw new Error("Invalid manifest.");
	}
	let streams = manifestDoc.querySelectorAll("MPD > Period > AdaptationSet");
	for (let s of streams) {
		mediaSummary.streams.push(await processStream(s));
	}

	async function processStream(streamRoot) {
		let ret = {};
		ret.contentType = streamRoot.getAttribute("contentType");
		if (!streamRoot.querySelector("ContentProtection")) {
			ret.unencrypted = true;
		} else {
			ret.encryptionType = streamRoot.querySelector("SegmentEncryption").getAttribute("schemeIdUri");
			if (ret.encryptionType !== "urn:mpeg:dash:sea:aes128-cbc:2013") {
				console.warn(`Warning: I do not recognize encryption schema ${ret.encryptionType}`);
			}
			let cryptoPeriogTag = streamRoot.querySelector("CryptoPeriod");
			if (cryptoPeriogTag) {
				let keyUrl = cryptoPeriogTag.getAttribute("keyUriTemplate");
				let h = new Headers();
				h.set("Authorization", `Bearer ${apiAccessToken}`);
				let keyRes = await fetch(keyUrl, {method: "GET", headers: h});
				if (keyRes.status !== 200) {
					throw new Error(`Unable to get key: server responded with ${keyRes.status}. url is ${keyUrl}`);
				}
				let key = await keyRes.blob();
				if (key.size !== 16) {
					throw new Error(`Invalid key length: got ${key.size}`);
				}
				let keyInHex = Array.prototype.map.call(new Uint8Array(await key.arrayBuffer()), byte => {
					return byte.toString(16).padStart(2, '0');
				}).join("");
				ret.key = keyInHex;
				ret.iv = cryptoPeriogTag.getAttribute("IV").replace(/^0x/, "");
			}
		}
		let reprs = streamRoot.querySelectorAll("Representation");
		if (reprs.length === 0) {
			throw new Error("No representation tags found.");
		}
		let maxBandwidth = 0;
		for (let repr of reprs) {
			let bw = parseInt(repr.getAttribute("bandwidth"));
			if (maxBandwidth < bw) {
				maxBandwidth = bw;
			}
		}
		let segTemplate = streamRoot.querySelector("SegmentTemplate");
		let mediaUrlTemplate = segTemplate.getAttribute("media").replace("$Bandwidth$", maxBandwidth.toString());
		ret.urlTemplate = manifestUrl.replace(/manifest.+$/, "") + mediaUrlTemplate;
		let timeline = segTemplate.querySelector("SegmentTimeline");
		let timesToGet = [0];
		for (let t of timeline.querySelectorAll("S")) {
			let d = parseInt(t.getAttribute("d"));
			let r = t.getAttribute("r");
			if (!r) {
				r = 0;
			} else {
				r = parseInt(r);
			}
			for (let i = 0; i < r + 1; i ++) {
				let nextTime = timesToGet[timesToGet.length - 1] + d;
				timesToGet.push(nextTime);
			}
		}
		timesToGet.splice(timesToGet.length - 1, 1);
		ret.timesToGet = timesToGet;
		return ret;
	}

	let a = document.createElement("a");
	a.href = URL.createObjectURL(new Blob([JSON.stringify(mediaSummary)]));
	a.download = `${videoId}.json`;
	a.click();
})(sessionInfo);
