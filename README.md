Download lecture video from MS Stream

Steps:

0. Install ffmpeg and openssl
1. Open video page
2. Press F12
3. Copy everything in `getinfo.js` and paste into JS console
4. Save resulting JSON file
5. `cd download`
6. `cargo run -- saved-json-file.json`
