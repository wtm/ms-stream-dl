use serde::Deserialize;
use openssl::symm;
use std::fs;
use std::io::Write;
use std::process::Command;

/// Convert a hex string with no whitespace or other sepreator into `&[u8]`.
pub fn hex_to_u8(hex: &str) -> Vec<u8> {
  if hex.len() % 2 != 0 {
    panic!("partial hex?");
  }
  let mut vec = Vec::new();
  vec.reserve(hex.len() / 2);
  for i in 0..(hex.len() / 2) {
    let hex = &hex[i*2..(i+1)*2];
    vec.push(u8::from_str_radix(hex, 16).unwrap());
  }
  vec
}

#[derive(Deserialize)]
struct MediaSummary {
	mainfest: String,
	streams: Vec<StreamSummary>,
	name: String
}

#[derive(Deserialize)]
struct StreamSummary {
	contentType: String,
	encryptionType: String,
	key: String,
	iv: String,
	urlTemplate: String,
	timesToGet: Vec<u64>
}

fn main() -> Result<(), &'static str> {
	let arg = std::env::args_os().skip(1).next().ok_or("expected 1 arg")?;
	let json = std::fs::read_to_string(arg).map_err(|_| "Unable to read json")?;
	let ms: MediaSummary = serde_json::from_str(&json).map_err(|_| "Unable to parse json.")?;

	println!("Ready to download: {}", ms.name);

	let mut folder_name = String::with_capacity(ms.name.len());
	if &ms.name == ".." || &ms.name == "." || &ms.name == "/" {
		return Err("Invalid name.");
	}
	for c in ms.name.chars() {
		if c == '/' || c == '\\' || c == '\0' || c.is_ascii_control() {
			println!("Warn: invalid character in name: {:?}", c);
		} else {
			folder_name.push(c);
		}
	}

	fs::create_dir(&folder_name).map_err(|_| "IO creating folder")?;
	std::env::set_current_dir(&folder_name).map_err(|_| "IO setting folder")?;

	let client = reqwest::blocking::ClientBuilder::new().build().map_err(|_| "Failed to build client")?;

	let mut stream_filenames: Vec<String> = Vec::new();

	for (i, s) in ms.streams.iter().enumerate() {
		println!("Downloading {} stream...", s.contentType);
		if &s.encryptionType != "urn:mpeg:dash:sea:aes128-cbc:2013" {
			return Err("Unknown encryption type.");
		}
		let key = hex_to_u8(&s.key);
		let iv = hex_to_u8(&s.iv);

		let fname = format!("{}.mp4", i);
		let mut decrypted_out = fs::File::create(&fname)
			.map_err(|_| "Unable to create output file.")?;
		stream_filenames.push(fname);
		let chunks = std::iter::once("i".to_owned()).chain(
			s.timesToGet.iter().map(|x| format!("{}", x))
		);
		for (i, chunk) in chunks.enumerate() {
			print!("\r\x1b[2K{}/{}", i, s.timesToGet.len() + 1);
			std::io::stdout().flush();
			let url = s.urlTemplate.replace("$Time$", &chunk);
			let dl = client.get(&url).send();
			match dl {
				Ok(k) => {
					let res = k.bytes().map_err(|_| "Getting response errored.")?;
					let decrypted = symm::decrypt(symm::Cipher::aes_128_cbc(), &key, Some(&iv), &*res)
						.map_err(|_| "Unable to decrypt.")?;
					decrypted_out.write_all(&decrypted).map_err(|_| "Unable to write.")?;
				},
				Err(e) => {
					eprintln!("Unable to download {}: {}", url, e);
					return Err("Download error.");
				}
			}
		}
		println!("");
	}

	println!("Combining files...");
	let mut args: Vec<String> = Vec::new();
	for f in stream_filenames {
		args.push("-i".to_owned());
		args.push(format!("file:{}", f));
	}
	args.extend_from_slice(&["-c".to_owned(), "copy".to_owned(), format!("file:{}.mp4", folder_name)]);
	if !Command::new("ffmpeg")
		.args(args)
		.spawn().map_err(|_| "Unable to run ffmpeg")?
		.wait().map_err(|_| "Unable to wait ffmpeg")?
		.success() {
			return Err("ffmpeg failed.");
		}

	Ok(())
}
